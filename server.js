// Port to listen requests from
var port = 1234;

// Modules to be used
const crypto = require('crypto')
var express = require("express");
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser')
var sqlite3 = require("sqlite3").verbose();
var app = express();
var db = new sqlite3.Database("db.sqlite");
const promisify = require('util').promisify
const db_get = promisify(db.get.bind(db));
const db_run = promisify(db.run.bind(db));
const db_all = promisify(db.all.bind(db));

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });


function getToken(ident) {
    const hash = crypto.createHash('sha256');
    hash.update(ident + crypto.randomBytes(48).toString('hex'));
    return hash.digest('hex')
}
app.use(cookieParser())
    // Log requests
app.all("*", urlencodedParser, function(req, res, next) {
    console.log(req.method + " " + req.url);
    console.dir(req.headers);
    console.log(req.body);
    console.log();
    next();
});

app.use(async function(req, res, next) {
    const $token = req.cookies.token;
    if ($token) {
        try {
            const data = await db_get("SELECT ident FROM sessions WHERE token = $token", { $token });
            req.user = data;
        } catch (e) {
            console.error(e);
        }
    }
    next();
});
// Serve static files
app.use(express.static("public"));
app.get('/me', (req, res) => {
    if (req.user) {
        res.json(req.user);
    } else {
        res.json(null);
    }
});
app.get("/login", (req, res) => res.redirect('/'));
app.get("/users", async function(req, res, next) {
    try {
        const data = await db_all("SELECT rowid, ident, password FROM users;");
        res.json(data);
    } catch (e) {
        conole.error(e);
        res.end();
    }
});
app.get('/logout', async(req, res) => {
    try {
        if (req.user) {
            res.clearCookie('token');
            const delete_result = await db_run('DELETE FROM sessions WHERE ident = ?', [req.user.ident]);
            res.redirect('/');
        }
    } catch (e) {
        console.error(e);
        res.end();
    }
})
app.post("/login", async function(req, res, next) {
    try {
        const { login, password } = req.body;
        const login_valid = await db_get(`SELECT ident FROM users WHERE ident = $login AND password = $password;`, { $login: login, $password: password });
        const status = !!login_valid;
        if (status) {
            const token = getToken(login);
            const data = await db_get("SELECT ident FROM sessions WHERE ident = ?", [login]);
            let stmt = "INSERT INTO sessions(ident, token) VALUES($login, $token);";
            if (data) stmt = "UPDATE sessions SET token = $token WHERE ident = $login;";
            const update_result = await db_run(stmt, { $login: login, $token: token });
            res.cookie('token', token)
            res.redirect('/');
        } else
            res.json({ status })
    } catch (e) {
        // console.error(e);
        res.json({ status: false })
    }
    // res.send("TODO");
});

// Startup server
app.listen(port, function() {
    console.log("Le serveur est accessible sur http://localhost:" + port + "/");
    console.log("La liste des utilisateurs est accessible sur http://localhost:" + port + "/users");
});